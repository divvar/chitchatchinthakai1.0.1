Rails.application.routes.draw do
  root 'chats#home'
  get '/login', to:'sessions#login'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
